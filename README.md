# ABOUT

## **M**inimal **U**nit **T**est for **C** (**MUTC**)
The goal of this project is to simplify the making of unit tests by being very easy to use.

|PROS|CONS
|-|-
|+ Easy to use | ~ Need a bit of setup
|+ Aestetic | ~ Need to compile
|+ Fast | ~ Need to download

## Syntax

Here is a very classical `main` function using the `Test Suite` system.
```c
int main() {

    TestSuite *tests = newTestSuite(); // We create the suite

    addFunction(tests, &test_creation, "My first function !"); 
    addFunction(tests, &test_basic_utilization, "My second test function :)" );

    runTestSuite(tests); // We run the suite

    return OK; // = 0
}
```
  
As a very quick example, here is the `test1` function :
```c
int test1() {
    
    float a = 1;
    float b = 3;
    int p = NULL;
    char easy[] = "easy";

    assertTrue(b > a); // pass
    assertFloat(a, b); // pass
    assertNull(p); // pass
    assertString(easy, "hard"); // fail

    return OK;
}
```

Here is an example of a `successful` suite :
```
[ 1 / 2 ] Test of Creation(): * -> OK
[ 2 / 2 ] Test of Basic scenario(): *********************************** -> OK
```

And now an example of a `failed` suite :
```
[ 1 / 2 ] Test of Creation(): * -> OK
[ 2 / 2 ] Test of Basic scenario(): ****************************x -> KO
---> ERROR at line n°72 (src/testqueue.c)
```

You can't see it in markdown but the keywords are highlighted in colors in order to find informations quickly.

## Example
You'll find a basic example `src/testqueue.c` folder

## Installation
The installation is very easy, you'll need to use this kind of option with GCC `-L{relative_path_of_libtest.a}` and`-ltest`. And you also need to link the `MUTC.h` to your project, you can find it in the `inc` folder.
The library is static, it means that you need to recompile it if you make any change.

## Bugs
Send an email to `antoine.burnel3@gmail.com` if you find a bug

## Roadmap
If you have any idea / feature that you want, send a mail to `antoine.burnel3@gmail.com`

## Contributing
There is no contribution

## Author
Antoine BURNEL

## License
A simple MIT license, check the `LICENSE.txt` file for more details

## Project status
This product is still in development