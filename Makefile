CC=gcc
RM=rm -f

inc=-Iinc
sources=src
headers=inc
objects=obj
binary=bin

CFLAGS=-pedantic -O3 -ansi -Wall -Wextra

assertion=assertion
queue=queue
testqueue=testqueue
test=test

outputdir=lib
nomLibTest=libtest.a

all: $(test).o $(queue).o $(assertion).o libtest $(testqueue).o

$(queue).o: $(sources)/$(queue).c
	$(CC) $(cflags) $(inc) $< -c -o $(objects)/$@

$(test).o: $(sources)/$(test).c
	$(CC) $(cflags) $(inc) $< -c -o $(objects)/$@

$(assertion).o: $(sources)/$(assertion).c
	$(CC) $(cflags) $(inc) $< -c -o $(objects)/$@

$(testqueue).o: $(sources)/$(testqueue).c
	$(CC) $(cflags) $(inc) $< -o $(binary)/$@ -L./lib -ltest

libtest: $(objects)/$(assertion).o $(objects)/$(queue).o $(objects)/$(test).o
	ar -rc $(outputdir)/$(nomLibTest) $^
	ranlib $(outputdir)/$(nomLibTest)

clean:
	$(RM) $(objects)/*.o
	$(RM) $(binary)/*
	$(RM) $(outputdir)/$(nomLibTest)