/**
 * @file test.h
 * @author Antoine (antoine.burnel3@gmail.com)
 * @brief This header contains all public functions's prototype usable for the unit tests
 * @version 0.1
 * @date 24/02/2023
 * @copyright Copyright (c) 2023
 */

#ifndef TEST_H
#define TEST_H

#define KO 1
#define OK 0
#define TRUE 1
#define FALSE 0
#define NOTNULL ((void *)1)
#define bool int
#define string char*

#define SET_COLOR_BLACK printf("\033[0;30m")
#define SET_COLOR_RED printf("\033[0;31m")
#define SET_COLOR_GREEN printf("\033[0;32m")
#define SET_COLOR_YELLOW printf("\033[0;33m")
#define SET_COLOR_BLUE printf("\033[0;34m")
#define SET_COLOR_PURPLE printf("\033[0;35m")
#define SET_COLOR_CYAN printf("\033[0;36m")
#define SET_COLOR_WHITE printf("\033[0;37m")
#define STOP_COLOR printf("\033[0m")

typedef int (*Function)(void);

#define assertTrue(res) assert_true(res, __LINE__, __FILE__)
#define assertFalse(res) assert_false(res, __LINE__, __FILE__)
#define assertNull(res) assert_null(res, __LINE__, __FILE__)
#define assertNotNull(res) assert_notNull(res, __LINE__, __FILE__)
#define assertEqual(a, b) assert_eq(a, b, __LINE__, __FILE__)
#define assertFloat(a, b) assert_eqf(a, b, __LINE__, __FILE__)
#define assertDouble(a, b) assert_eqd(a, b, __LINE__, __FILE__)
#define assertPointer(a, b) assert_eqp(a, b, __LINE__, __FILE__)
#define assertString(a, b) assert_eqs(a, b, __LINE__, __FILE__)

/**
 * @brief Assert an existing function
 * 
 * @param function The adress of the function to assert
 * @param name_method The name that will be displayed in the terminal
 */
void assert(Function function, string name_method);

/**
 * @brief Assert an existing function but it print the test number
 * 
 * @param function The address of the function to test
 * @param name_method The title that will be printed on the screen
 * @param index The index of the current method
 * @param maxIndex The index of the last element of the suite
 */
void assertAdvanced(bool (*function)(), string name_method, int index, int maxIndex);

/**
 * @brief Exit the tests if the given result is FALSE
 * 
 * @param res If the result is FALSE, it is the end, else we continue normally
 */
void assert_true(bool res, int line, string file);

/**
 * @brief Exit the tests if the given result is TRUE
 * 
 * @param res If the result is TRUE, it is the end, else we continue normally
 */
void assert_false(bool res, int line, string file);

/**
 * @brief Exit the tests if the given adress is NOT NULL
 * 
 * @param res If the result is NOT NULL, it is the end, else we continue normally
 */
void assert_null(void *res, int line, string file);

/**
 * @brief Exit the tests if the given adress is NULL
 * 
 * @param res If the result is NULL, it is the end, else we continue normally
 */
void assert_notNull(void* res, int line, string file);

/**
 * @brief Exit the tests the two integers are not equal
 * 
 * @param a The first integer
 * @param b The second integer
 */
void assert_eq(int a, int b, int line, string file);

/**
 * @brief Exit the tests if the two float are not equal
 * 
 * @param a The first float
 * @param b The second float
 */
void assert_eqf(float a, float b, int line, string file);

/**
 * @brief Exit the tests if the two double are not equal
 * 
 * @param a The first double
 * @param b The second double
 */
void assert_eqd(double a, double b, int line, string file);

/**
 * @brief Exit the tests if the two pointers are not pointing the same address
 * 
 * @param a The first pointer
 * @param b The second pointer
 */
void assert_eqp(void *a, void *b, int line, string file);

/**
 * @brief Exit the tests if the two string are not equals
 * 
 * @param a The first string
 * @param b The second string
 */
void assert_eqs(string a, string b, int line, string file);

#endif