#ifndef ASSERTION_H
#define ASSERTION_H

#include "queue.h"

TestSuite *newTestSuite();

void addFunction(TestSuite *q, Function function, string title);

void runTestSuite(TestSuite *q);

#endif