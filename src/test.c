#include "test.h"
#include <stdlib.h>
#include <stdio.h>

void priv_print_error() {
    SET_COLOR_RED;
    printf("x");
    STOP_COLOR;
}

void priv_print_advancement() {
    SET_COLOR_WHITE;
    printf("*");
    STOP_COLOR;
}

void priv_print_failure(int line, string file) {
    printf(" -> ");
    SET_COLOR_RED;
    printf("KO\n");
    printf("---> ERROR");
    STOP_COLOR;
    printf(" at line n°");
    SET_COLOR_YELLOW;
    printf("%d", line);
    STOP_COLOR;
    printf(" (");
    SET_COLOR_YELLOW;
    printf("%s", file);
    STOP_COLOR;
    printf(")\n");
}

void priv_print_failure_without_line() {
    printf(" -> ");
    SET_COLOR_RED;
    printf("KO\n");
    STOP_COLOR;
    printf("The asserted function returned [");
    SET_COLOR_RED;
    printf("KO");
    STOP_COLOR;
    printf("]\n");
}

void priv_print_success() {
    printf(" -> ");
    SET_COLOR_GREEN;
    printf("OK\n");
    STOP_COLOR;
}

void priv_print_index(int index, int maxIndex) {
    printf("[ ");
    SET_COLOR_CYAN;
    printf("%d", index);
    STOP_COLOR;
    printf(" / ");
    SET_COLOR_CYAN;
    printf("%d", maxIndex);
    STOP_COLOR;
    printf(" ] ");
}

void priv_print_methodname(string name_method) {
    printf("Test of ");
    SET_COLOR_YELLOW;
    printf("%s", name_method);
    STOP_COLOR;
    printf("(): ");
}

void* priv_inverse(void* p) {
    if (p == NULL) return NOTNULL;
    return NULL;
}

bool compareTwoString(string a, string b) {
    int i = 0;
    while (a[i] == b[i]) {
        if (a[i] == '\0') {
            return TRUE;
        }
        i++;
    }
    return FALSE;
}

void assert(bool (*function)(), string name_method) {
    int res;
    priv_print_methodname(name_method);
    res = (*function)();
    if (res == KO) {
        priv_print_failure_without_line();
        exit(EXIT_FAILURE);
    }
    priv_print_success();
}

void assertAdvanced(bool (*function)(), string name_method, int index, int maxIndex) {
    int res;
    priv_print_index(index, maxIndex);
    priv_print_methodname(name_method);
    res = (*function)();
    if (res == KO) {
        priv_print_failure_without_line();
        exit(EXIT_FAILURE);
    }
    priv_print_success();
}

void assert_true(bool res, int line, string file) {
    if (res == FALSE) {
        priv_print_error();
        priv_print_failure(line, file);
        exit(EXIT_FAILURE);
    }
    priv_print_advancement();
}

void assert_false(bool res, int line, string file) {
    assert_true(!res, line, file);
}

void assert_null(void *res, int line, string file) {
    if (res != NULL) {
        priv_print_error();
        priv_print_failure(line, file);
        exit(EXIT_FAILURE);
    }
    priv_print_advancement();
}

void assert_notNull(void *res, int line, string file) {
    assert_null(priv_inverse(res), line, file);
}

void assert_eq(int a, int b, int line, string file) {
    if (a != b) {
        priv_print_error();
        priv_print_failure(line, file);
        exit(EXIT_FAILURE);
    }
    priv_print_advancement();
}

void assert_eqf(float a, float b, int line, string file) {
    if (abs(a - b) > __FLT_EPSILON__) {
        priv_print_error();
        priv_print_failure(line, file);
        exit(EXIT_FAILURE);
    }
    priv_print_advancement();
}

void assert_eqd(double a, double b, int line, string file) {
    if (abs(a - b) > __DBL_EPSILON__) {
        priv_print_error();
        priv_print_failure(line, file);
        exit(EXIT_FAILURE);
    }
    priv_print_advancement();
}

void assert_eqp(void *a, void *b, int line, string file) {
    if (a != b) {
        priv_print_error();
        priv_print_failure(line, file);
        exit(EXIT_FAILURE);
    }
    priv_print_advancement();
}

void assert_eqs(string a, string b, int line, string file) {
    if (!compareTwoString(a, b)) {
        priv_print_error();
        priv_print_failure(line, file);
        exit(EXIT_FAILURE);
    }
    priv_print_advancement();
}
