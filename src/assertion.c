#include "assertion.h"

TestSuite *newTestSuite() {
    return createQueue();
}

void addFunction(TestSuite *q, Function function, string title) {
    enqueue(q, function, title);
}

void runTestSuite(TestSuite *q) {
    int initialNumberOftests = q->count;
    string bufferTitle;
    int indexTest = 0;
    Function currentFunction;
    while (!isQueueEmpty(*q)) {
        currentFunction = dequeue(q, &bufferTitle);
        assertAdvanced(currentFunction, bufferTitle, indexTest+1, initialNumberOftests);
        free(bufferTitle);
        indexTest++;
    }
    freeQueue(q);
}