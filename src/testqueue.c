#include <stdio.h>
#include "MUTC.h"

bool function1 ( void ) {
    return OK;
}

bool function2 ( void ) {
    return OK;
}

bool function3 ( void ) {
    return OK;
}

bool test_creation ( void ) {
    TestSuite *q = createQueue();
    freeQueue(q);
    assertNotNull(q);
    return OK;
}

bool test_normal_scenario ( void ) {
    string buffer;
    TestSuite* q = createQueue();
    assertTrue( isQueueEmpty(*q) );
    
    enqueue( q, &function1, "f1" );
    assertFalse( isQueueEmpty(*q) );
    assertPointer( queueGetFrontValue(*q, &buffer), &function1 );
    assertString( buffer, "f1");
    assertNotNull( q->front );
    assertNotNull( q->rear );

    assertPointer( dequeue(q, &buffer), &function1 );
    assertString( buffer, "f1");
    free(buffer);
    assertTrue( isQueueEmpty(*q) );
    assertNull( q->front );
    assertNull( q->rear );

    enqueue(q, &function1, "f1");
    assertFalse( isQueueEmpty(*q) );
    assertPointer( queueGetFrontValue(*q, &buffer), &function1 );
    assertString( buffer, "f1" );

    enqueue(q, &function2, "f2");
    assertFalse( isQueueEmpty(*q) );
    assertPointer( queueGetFrontValue(*q, &buffer), &function1 );
    assertString( buffer, "f1" );

    enqueue(q, &function3, "f3");
    assertFalse( isQueueEmpty(*q) );
    assertPointer( queueGetFrontValue(*q, &buffer), &function1 );
    assertString( buffer, "f1" );

    assertPointer( dequeue(q, &buffer), &function1 );
    assertString( buffer, "f1" );
    free(buffer);
    assertFalse( isQueueEmpty(*q) );
    assertNotNull( q->front );
    assertNotNull( q->rear );

    assertPointer( dequeue(q, &buffer), &function2 );
    assertString( buffer, "f2" );
    free(buffer);
    assertFalse( isQueueEmpty(*q) );
    assertNotNull( q->front );
    assertNotNull( q->rear );

    assertPointer( dequeue(q, &buffer), &function3 );
    assertString( buffer, "f3" );
    free(buffer);
    assertTrue( isQueueEmpty(*q) );
    assertNull( q->front );
    assertNull( q->rear );

    freeQueue( q );

    return OK;
}

bool main() {

    TestSuite *tests = newTestSuite();
    addFunction(tests, &test_creation, "Creation");
    addFunction(tests, &test_normal_scenario, "Basic scenario" );
    runTestSuite(tests);

    return OK;
}