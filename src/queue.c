#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"

nodeQueue * priv_nodeQueue_create(Function function, string title) {
    nodeQueue *nq = malloc(sizeof(nodeQueue));
    nq->function = function;
    nq->title = malloc(sizeof(char) * ASSERT_TITLE_SIZE);
    if (strncpy(nq->title, title, ASSERT_TITLE_SIZE) == NULL) return NULL;
    nq->next = NULL;
    return nq;
}

/**
 * @brief Creates a new, empty queue.
 *
 * @return A pointer to the newly created queue.
 */
TestSuite * createQueue() {
    TestSuite *q = malloc(sizeof(TestSuite));
    q->front = NULL;
    q->rear = NULL;
    q->count = 0;
    return q;
}

/**
 * @brief Prints all the elements in the queue, from front to rear.
 *
 * @param q The queue to print.
 */
void queuePrint(TestSuite q) {
    nodeQueue *cursor = q.front;
    printf("[");
    while (cursor != NULL) {
        printf("%p : %s", (void*) cursor->function, cursor->title);
        if (cursor->next != NULL) printf(", ");
        cursor = cursor->next;
    }
    printf("]");
}


/**
 * @brief Adds an element to the rear of the queue.
 *
 * @param q A pointer to the queue to add the element to.
 * @param data The data to be added to the queue.
 */
void enqueue(TestSuite* q, Function data, string title) {
    nodeQueue *nq = priv_nodeQueue_create(data, title);
    q->count++;
    if (isQueueEmpty(*q)) {
        q->front = nq;
        q->rear = nq;
        return;
    }
    q->rear->next = nq;
    q->rear = nq;
}

/**
 * @brief Removes and returns the front element of the queue.
 *
 * @param q A pointer to the queue to remove the front element from.
 * @return The data stored in the front element of the queue.
 */
Function dequeue(TestSuite* q, string *title) {
    Function dataToReturn;
    nodeQueue *next;

    if (isQueueEmpty(*q)) {
        return NULL;
    }

    dataToReturn = q->front->function;
    *title = q->front->title;
    next = q->front->next;
    free(q->front);

    if (next == NULL) {
        q->front = NULL;
        q->rear = NULL;
        return dataToReturn;
    }

    q->front = next;
    q->count--;
    return dataToReturn;
}

/**
 * @brief Checks whether the queue is empty.
 *
 * @param q The queue to check.
 * @return 1 if the queue is empty, 0 otherwise.
 */
int isQueueEmpty(TestSuite q) {
    return q.front == NULL;
}

/**
 * @brief Returns the value of the front node but doen't modify the queue
 *
 * @param q A pointer to the queue to get the size of.
 * @return The number of elements in the queue.
 */
Function queueGetFrontValue(TestSuite q, string *title) {
    *title = q.front->title;
    return q.front->function;
}

void freeNodeQueue(nodeQueue *nq) {
    if (nq == NULL) return;
    freeNodeQueue(nq->next);
    free(nq->title);
}

void freeQueue(TestSuite *q) {
    freeNodeQueue(q->front);
    free(q);
}